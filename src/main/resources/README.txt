Welcome and thanks for using FEED BUSTER.

To use this application you will not need to configure anything, just start the application via IDE or execute the jar file.
After startup a script sql is loaded. (src/main/resources/import.sql).

Feed buster works with every possible feed source in XML format.

HOW IT WORKS:
After startup a job is configured to refresh stored feeds for updated entries, every specified minutes interval (DEFAULT: 1 minute - configurable via application.properties)
At the begin no feed are stored. When the job run first, if no feeds are stored, it initialize the application with a default source (configurable via application.properties).
After this phase check every stored feed for updates, and stores the retrieved entries in the database. (I used H2 for demo, but it can be very easily replaced.)

After the default specified timeout in application.properties, you will see the entries start populating from default feed source.

This web application run on your browser, at the url: http://localhost:8080/ 

You can query the database and see its status at the url: http://localhost:8080/h2-console (User: SA, no password needed)

All the services use a DTO, FeedBusterServiceResponse, to comunicate with the incoming request.

The services implemented are:

---> http://localhost:8080/feeds GET -> retrieve a JSON with last 20 stored feeds, mixed between sources and ordered by publication date.
     You can alternatively use:
---> http://localhost:8080/feeds?max_results=[MAX_RESULTS_NUMBER], -int- , the max number of results.
---> http://localhost:8080/feeds/add?format=XML&feed_url=[FEED_SORCE_URL] POST -> add a new feed with url and format type
---> http://localhost:8080/feeds/refresh PUT -> update the stored content trough ALL STORED feeds.
---> http://localhost:8080/feeds/project GET -> retrieve a Json containing a list of the used technology stack. 

The services can be tested trough SWAGGER http://localhost:8080/swagger-ui.html#/feed-buster-controller or using applications like Postman.


