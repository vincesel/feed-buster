package com.feed.buster.exceptions;

import static com.feed.buster.model.MessageConstants.*;
import com.feed.buster.model.response.ResponseCode;
import com.feed.buster.model.response.ResponseExitStatus;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 *
 * @author vincenzo
 */
@RestControllerAdvice
public class ExceptionProcessor {
    
    private static final Logger LOGGER = Logger.getLogger(ExceptionHandler.class.getName());

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public FeedBusterServiceResponse missingRequiredParameters(Exception ex)
    {
        LOGGER.log(Level.SEVERE, "Intercepting MissingServletRequestParameterException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(MISSING_SERVLET_REQUEST_PARAMETER_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public FeedBusterServiceResponse requestMethodNotSupported(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting HttpRequestMethodNotSupportedException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(HTTP_REQUEST_METHOD_NOT_SUPPORTED_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(MalformedFeedUrlException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public FeedBusterServiceResponse processMalformedFeedUrl(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting MalformedFeedUrlException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(MALFORMED_FEED_URL_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FeedBusterServiceResponse processRuntimeException(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting RuntimeException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(RUNTIME_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(UnknownHostException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public FeedBusterServiceResponse processUnknownHostException(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting UnknownHostException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(UNKNOWN_HOST_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(FeedReaderCreationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FeedBusterServiceResponse processFeedReaderCreationException(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting FeedReaderCreationException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList(FEED_READER_CREATION_EXCEPTION_MESSAGE, ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
    
    @ExceptionHandler(UnsupportedFeedFormatException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FeedBusterServiceResponse processUnsupportedFeedFormatException(Exception ex) {
        LOGGER.log(Level.SEVERE, "Intercepting UnsupportedFeedFormatException.");
        
        ResponseExitStatus responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withResponseCode(ResponseCode.ERROR)
                .withMessages(Arrays.asList("Unsupported stream format.", ex.getMessage()))
                .build();
        
        FeedBusterServiceResponse response = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .build();
        
        return response;
    }
}
