package com.feed.buster.exceptions;

/**
 *
 * @author vincenzo
 */
public class MalformedFeedUrlException extends RuntimeException {

    public MalformedFeedUrlException() {
    }

    public MalformedFeedUrlException(String msg) {
        super(msg);
    }
}
