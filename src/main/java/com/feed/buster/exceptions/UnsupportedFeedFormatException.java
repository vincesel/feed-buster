package com.feed.buster.exceptions;

/**
 *
 * @author vincenzo
 */
public class UnsupportedFeedFormatException extends RuntimeException {
    
    public UnsupportedFeedFormatException() {
    }

    public UnsupportedFeedFormatException(String msg) {
        super(msg);
    }
}
