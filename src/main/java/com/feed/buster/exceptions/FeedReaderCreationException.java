package com.feed.buster.exceptions;

/**
 *
 * @author vincenzo
 */
public class FeedReaderCreationException extends RuntimeException {

    public FeedReaderCreationException() {
    }

    public FeedReaderCreationException(String msg) {
        super(msg);
    }
}
