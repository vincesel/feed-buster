package com.feed.buster;

import com.sun.syndication.io.FeedException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.feed.buster")
public class FeedBusterStarter {

	public static void main(String[] args) throws IOException, IllegalArgumentException, MalformedURLException, FeedException {
		SpringApplication.run(FeedBusterStarter.class, args);
	}
}
