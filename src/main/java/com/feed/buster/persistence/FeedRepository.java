package com.feed.buster.persistence;

import com.feed.buster.persistence.dto.FeedDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vincenzo
 */
@Repository
public interface FeedRepository extends JpaRepository <FeedDTO, String> {
    
}
