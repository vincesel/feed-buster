package com.feed.buster.persistence;

import com.feed.buster.persistence.dto.FeedEntryDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vincenzo
 */
@Repository
public interface FeedEntriesRepository extends JpaRepository <FeedEntryDTO, String> {
    
}
