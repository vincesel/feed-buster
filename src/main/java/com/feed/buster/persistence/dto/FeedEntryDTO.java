package com.feed.buster.persistence.dto;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author vincenzo
 */
@Entity(name="FeedEntries")
public class FeedEntryDTO {
    
    @Id
    private String resourceUri;
    private String title;
    @Column(columnDefinition="TEXT")
    private String description;
    private Date publicationDate;
    private String imageUrl;
    private String sourceId;
    
    public FeedEntryDTO() {};

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFeedId() {
        return sourceId;
    }

    public void setFeedId(String sourceId) {
        this.sourceId = sourceId;
    }
    
}
