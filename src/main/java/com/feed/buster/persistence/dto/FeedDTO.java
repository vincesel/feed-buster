package com.feed.buster.persistence.dto;

import com.feed.buster.model.FeedFormatType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author vincenzo
 */
@Entity(name="Feeds")
public class FeedDTO implements Serializable {
    
    @Id
    private String url;
    private FeedFormatType formatType;

    public FeedDTO() {};
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FeedFormatType getFormatType() {
        return formatType;
    }

    public void setFormatType(FeedFormatType formatType) {
        this.formatType = formatType;
    }
    
}
