package com.feed.buster.persistence.dao;

import com.feed.buster.model.FeedEntry;
import com.feed.buster.persistence.converters.FeedEntryDTOConverter;
import com.feed.buster.persistence.dto.FeedEntryDTO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.feed.buster.persistence.FeedEntriesRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 *
 * @author vincenzo
 */
@Repository
public class FeedEntriesDAO {
    
    private static final Logger LOGGER = Logger.getLogger(FeedEntriesDAO.class.getName());

    @Autowired
    FeedEntriesRepository feedEntriesRepository;
    
    @Value("${com.feed.buster.persistence.dao.feedentriesdao.default.max.results}")
    private Integer DEFAULT_MAX_RESULTS;
    
    public List<FeedEntry> getStoredEntries(Integer maxResults) {
        Integer resultLimit = maxResults != null ? maxResults : DEFAULT_MAX_RESULTS;
        List<FeedEntryDTO> storedEntries = feedEntriesRepository.findAll(PageRequest.of(0, resultLimit, Sort.by(Sort.Direction.DESC, "publicationDate")))
                .getContent(); 
        
        LOGGER.log(Level.INFO, "Retrieved entries: {0}", storedEntries.size());
        
        List<FeedEntry> results = new ArrayList<>();
        
        storedEntries.stream()
                .forEach(entry -> results.add(FeedEntryDTOConverter.getEntryFrom(entry)));
        
        return results;
    }
    
     public List<FeedEntry> getStoredEntries() {
        return getStoredEntries(DEFAULT_MAX_RESULTS);
    }

    public void saveFeedEntries(List<FeedEntry> feedEntriesList) {
        List<FeedEntryDTO> feedsDTOList = new ArrayList<>();
        
        feedEntriesList.stream()
                .forEach(feedEntry -> feedsDTOList.add(FeedEntryDTOConverter.getDtoFrom(feedEntry)));
        
        feedEntriesRepository.saveAll(feedsDTOList);
        feedEntriesRepository.flush();
    }
   
}
