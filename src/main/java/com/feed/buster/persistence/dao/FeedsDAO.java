package com.feed.buster.persistence.dao;

import com.feed.buster.model.Feed;
import com.feed.buster.persistence.converters.FeedDTOConverter;
import com.feed.buster.persistence.dto.FeedDTO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.feed.buster.persistence.FeedRepository;

/**
 *
 * @author vincenzo
 */
@Repository
public class FeedsDAO {
   
    @Autowired
    private FeedRepository feedsRepository;
    
    public List<Feed> getFeeds() {
        List<FeedDTO> feedsDTOList = feedsRepository.findAll();
        List<Feed> sourcesList = new ArrayList<>();
        
        feedsDTOList.stream()
                .forEach(source -> sourcesList.add(FeedDTOConverter.getSourceFrom(source)));
   
        return sourcesList;
    }
    
    public void saveFeed(Feed feed) {
        FeedDTO feedDTO = FeedDTOConverter.getDtoFrom(feed);
        feedsRepository.saveAndFlush(feedDTO);
    }
    
}
