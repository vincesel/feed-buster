package com.feed.buster.persistence.converters;

import com.feed.buster.model.Feed;
import com.feed.buster.persistence.dto.FeedDTO;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class FeedDTOConverter {
    
    public static FeedDTO getDtoFrom(Feed source) {
        FeedDTO sourceDTO = new FeedDTO();
        
        sourceDTO.setFormatType(source.getFormatType());
        sourceDTO.setUrl(source.getUrl());
        
        return sourceDTO;
    }
    
    public static Feed getSourceFrom(FeedDTO sourceDTO) {
        Feed source = new Feed.FeedBuilder()
                .withFormatType(sourceDTO.getFormatType())
                .withUrl(sourceDTO.getUrl())
                .build();
        
        return source;
    }
}
