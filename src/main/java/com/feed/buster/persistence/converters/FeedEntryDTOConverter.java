package com.feed.buster.persistence.converters;

import com.feed.buster.model.FeedEntry;
import com.feed.buster.persistence.dto.FeedEntryDTO;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public abstract class FeedEntryDTOConverter {
    
    public static FeedEntryDTO getDtoFrom(FeedEntry feedEntry) {
        FeedEntryDTO feedEntryDTO = new FeedEntryDTO();
        
        feedEntryDTO.setDescription(feedEntry.getDescription());
        feedEntryDTO.setImageUrl(feedEntry.getImageUrl());
        feedEntryDTO.setPublicationDate(feedEntry.getPublicationDate());
        feedEntryDTO.setResourceUri(feedEntry.getResourceUri());
        feedEntryDTO.setFeedId(feedEntry.getSourceId());
        feedEntryDTO.setTitle(feedEntry.getTitle());
        
        return feedEntryDTO;
    }
    
    public static FeedEntry getEntryFrom(FeedEntryDTO feedEntryDto) {
        FeedEntry feedEntry = new FeedEntry.FeedEntryBuilder()
                .withDescription(feedEntryDto.getDescription())
                .withImageUrl(feedEntryDto.getImageUrl())
                .withPubblicationDate(feedEntryDto.getPublicationDate())
                .withResourceUri(feedEntryDto.getResourceUri())
                .withSourceId(feedEntryDto.getFeedId())
                .withTitle(feedEntryDto.getTitle())
                .build();
        
        return feedEntry;
    }

}