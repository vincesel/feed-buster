package com.feed.buster.services;

import com.feed.buster.model.Feed;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.persistence.dao.FeedsDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class DefineDefaultFeedSourceService {
    
    private static final Logger LOGGER = Logger.getLogger(DefineDefaultFeedSourceService.class.getName());
    
    @Value("${com.feed.buster.services.default.feed.source.url}")
    private String defaultSourceFeedUrl;
    
    @Value("${com.feed.buster.services.default.feed.source.format}")
    private String defaultSourceFeedFormat;
    
    @Autowired
    private FeedsDAO feedsDao;
    
    private Feed defaultFeed;
    
    public Feed configureApplicationDefaultSourceFeed() {
        LOGGER.log(Level.INFO, "Instantiating default source feed.");
        buildFeedToStore();
        storeDefaultFeedSource();
        
        return defaultFeed;
    }
    
    private void buildFeedToStore() {
        FeedFormatType feedFormatType = FeedFormatType.valueOf(defaultSourceFeedFormat);
        
        defaultFeed = new Feed.FeedBuilder()
                .withFormatType(feedFormatType)
                .withUrl(defaultSourceFeedUrl)
                .build();
    }
    
    private void storeDefaultFeedSource() {
        feedsDao.saveFeed(defaultFeed);
        
        LOGGER.log(Level.INFO, "Default source feed successfully stored.");
    }
    
}
