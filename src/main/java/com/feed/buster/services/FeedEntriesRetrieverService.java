package com.feed.buster.services;

import com.feed.buster.interfaces.FeedBusterPayload;
import com.feed.buster.model.FeedEntry;
import com.feed.buster.model.response.FeedBusterPayloadImpl;
import com.feed.buster.model.response.ResponseCode;
import com.feed.buster.model.response.ResponseExitStatus;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import com.feed.buster.persistence.dao.FeedEntriesDAO;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class FeedEntriesRetrieverService {
    
    private static final Logger LOGGER = Logger.getLogger(FeedEntriesRetrieverService.class.getName());
    
    @Autowired
    private FeedEntriesDAO feedEntriesDAO;
    
    private Integer maxResults;
    private List<FeedEntry> feedEntriesList;
    private ResponseExitStatus responseStatus;
    private FeedBusterServiceResponse responseWrapper;
    
    public FeedBusterServiceResponse getStoredFeedEntries(Integer maxResults) {
        initParam(maxResults);
        retrieveStoredEntries();
        buildServiceResponse();
        return responseWrapper;
    }
    
    private void initParam(Integer maxResults) {
        this.maxResults = maxResults;
    }
    
    private void retrieveStoredEntries() {
        feedEntriesList = feedEntriesDAO.getStoredEntries(maxResults);
        LOGGER.log(Level.INFO, "Retrieved entries: {0}", feedEntriesList.size());
    }
    
    private void buildServiceResponse() {
        responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withMessages(Arrays.asList("Entries successfully retrieved."))
                .withResponseCode(ResponseCode.OK)
                .build();
        
        FeedBusterPayload servicePayload = new FeedBusterPayloadImpl.PayloadBuilder()
                .withPayload(feedEntriesList)
                .build();
        
        Optional payload = Optional.of(servicePayload);
        
        responseWrapper = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withPayload(payload)
                .withStatus(responseStatus)
                .build();
    }
}
