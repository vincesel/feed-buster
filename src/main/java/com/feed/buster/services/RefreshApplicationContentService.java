package com.feed.buster.services;

import com.feed.buster.interfaces.FeedStreamReader;
import com.feed.buster.model.FeedEntry;
import com.feed.buster.model.Feed;
import com.feed.buster.model.response.ResponseCode;
import com.feed.buster.model.response.ResponseExitStatus;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.feed.buster.persistence.dao.FeedEntriesDAO;
import com.feed.buster.persistence.dao.FeedsDAO;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.swing.Timer;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author vincenzo
 */
@Service
public class RefreshApplicationContentService {
    
    private static final Logger LOGGER = Logger.getLogger(RefreshApplicationContentService.class.getName());
    
    @Value("${com.feed.buster.services.refresh.interval.minutes}")
    private int REFRESH_INTERVAL_MINUTES;
    private static final int MILLISECONDS_PER_MINUTE = 60000;

    @Autowired
    private FeedsDAO sourcesDao;
    @Autowired
    private FeedEntriesDAO feedsDao;
    @Autowired
    private DefineDefaultFeedSourceService defineDefaultFeedSourceService;
    @Autowired
    private FeedReaderFactory feedReaderFactory;
    
    private static Timer refreshContentScheduledTimer;
    
    private List<Feed> feeds;
    private List<FeedEntry> feedEntries;
    private SyndFeed readedFeed;
    private String feedUrl;
    
    private ResponseExitStatus exitStatus;
    private FeedBusterServiceResponse responseWrapper;
    
    private final static String HTML_TAGS_REMOVER_REG_EXP =  "<[^>]*>";
    private final static String BACKSLASH_ESCAPE_REMOVER_REG_EXP = "\\\"";
    private final static String NEW_LINE_ESCAPE_REMOVER_REG_EXP = "\\n";
    
    @PostConstruct
    private void scheduleWork() {
        int millisecondsRefreshInterval = getMillisecondsFromMinutes(REFRESH_INTERVAL_MINUTES);
        
        refreshContentScheduledTimer = new Timer(millisecondsRefreshInterval, (ActionEvent arg0) -> {
            refreshApplicationData();
        });
        refreshContentScheduledTimer.setRepeats(true);
        refreshContentScheduledTimer.start();
        
        LOGGER.log(Level.INFO, "Refresh successfully scheduled every {0} milliseconds.", millisecondsRefreshInterval);
    }
    
    private int getMillisecondsFromMinutes(int refreshIntervalInMinutes) {
        return MILLISECONDS_PER_MINUTE * refreshIntervalInMinutes;
    }
    
    public FeedBusterServiceResponse refreshApplicationData() {
        LOGGER.log(Level.INFO, "Refreshing data...");
        initParams();
        getSourcesToRead();
        ensureDefaultSourceIsPresent();
        readEntriesFromRetrievedSources();
        storeRetrievedFeedEntries();
        buildServiceResponse();
        
        return responseWrapper;
    }
    
    private void initParams() {
        this.feeds = new ArrayList<>();
        this.feedEntries = new ArrayList<>();
        this.readedFeed = null;
        this.feedUrl = null;
    }
    
    private void getSourcesToRead() {
        this.feeds = sourcesDao.getFeeds();
        LOGGER.log(Level.INFO, "Retrieved feeds: {0} ", feeds.size());
    }
    
    private void ensureDefaultSourceIsPresent() {
        if (feeds.isEmpty()) {
            Feed defaultSourceFeed = defineDefaultFeedSourceService.configureApplicationDefaultSourceFeed();
            feeds.add(defaultSourceFeed);
        }  
    }
    
    private void readEntriesFromRetrievedSources() {
        this.feeds.forEach(feed -> readEntriesFrom(feed));
    }
    
    private void readEntriesFrom(Feed source) {
        FeedStreamReader streamReader = feedReaderFactory.getReader(source.getFormatType());
        this.readedFeed = streamReader.getFeedFrom(source.getUrl());
        this.feedUrl = source.getUrl();
        buildFeedEntriesFromActualSource();
    }
    
    private void buildFeedEntriesFromActualSource() {
        this.readedFeed.getEntries().stream()
                .forEach(entry -> addToFeedsEntriesList((SyndEntryImpl) entry));
    }
    
    private void addToFeedsEntriesList(SyndEntryImpl entry) {
        FeedEntry feedEntry = buildFeedEntryObjectFromReadedEntry(entry);
        this.feedEntries.add(feedEntry);
    }
    
    public FeedEntry buildFeedEntryObjectFromReadedEntry(SyndEntryImpl entry) {
        String mediaUri = "";
        
        if (!entry.getEnclosures().isEmpty()) {
            SyndEnclosureImpl enclosure = (SyndEnclosureImpl) entry.getEnclosures().get(0);
            mediaUri = enclosure.getUrl();
        }
        
        
        return new FeedEntry.FeedEntryBuilder()
            .withTitle(entry.getTitle())
            .withDescription(
                    removeSpecialCharsFromString(entry.getDescription().getValue())
            )
            .withResourceUri(entry.getUri())
            .withImageUrl(mediaUri)
            .withPubblicationDate(entry.getPublishedDate())
            .withSourceId(feedUrl)
            .build();
    }
    
    private String removeSpecialCharsFromString(String inputString) {
        
        return inputString
                .replaceAll(HTML_TAGS_REMOVER_REG_EXP, "")
                .replaceAll(BACKSLASH_ESCAPE_REMOVER_REG_EXP, "\"")
                .replaceAll(NEW_LINE_ESCAPE_REMOVER_REG_EXP, " ");
    }
    
    private void storeRetrievedFeedEntries() {
        feedsDao.saveFeedEntries(feedEntries);
        LOGGER.log(Level.INFO, "Entries updated.");
    }
    
    private void buildServiceResponse() {
        exitStatus = new ResponseExitStatus.ResponseStatusBuilder()
            .withMessages(Arrays.asList("Content refreshed."))
            .withResponseCode(ResponseCode.OK)
            .build();

        responseWrapper = new FeedBusterServiceResponse.ResponseWrapperBuilder()
            .withStatus(exitStatus)
            .withPayload(Optional.empty())
            .build();
    }
    
    public Timer getScheduledWork() {
        return refreshContentScheduledTimer;
    }
}
