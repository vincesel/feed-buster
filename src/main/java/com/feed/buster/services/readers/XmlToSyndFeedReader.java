package com.feed.buster.services.readers;

import com.feed.buster.exceptions.FeedReaderCreationException;
import com.feed.buster.exceptions.MalformedFeedUrlException;
import com.feed.buster.interfaces.FeedStreamReader;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class XmlToSyndFeedReader implements FeedStreamReader {

    private static final Logger LOGGER = Logger.getLogger(XmlToSyndFeedReader.class.getName());
    
    @Override
    public SyndFeed getFeedFrom(String sourceURL) {
        URL feedSource = null;
        try {
            feedSource = new URL(sourceURL);
        } 
        catch (MalformedURLException ex) {
            throw new MalformedFeedUrlException(ex.getMessage());
        }
        
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = null;
        try {
            feed = input.build(new XmlReader(feedSource));
        }
        catch (IOException | IllegalArgumentException | FeedException ex) {
            throw new FeedReaderCreationException(ex.getMessage());
        }
       
        LOGGER.log(Level.INFO, "Reader instantiated.");
        return feed;
    }
    
}
