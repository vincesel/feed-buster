package com.feed.buster.services;

import org.springframework.stereotype.Service;
import com.feed.buster.interfaces.ApplicationManager;
import com.feed.buster.model.Feed;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vincenzo
 */
@Service
public class FeedBusterManager implements ApplicationManager {
    
    @Autowired
    RefreshApplicationContentService refreshApplicationContentService;
    @Autowired
    FeedInsertService feedInsertService;
    @Autowired
    FeedEntriesRetrieverService entriesRetrieverService;

    @Override
    public FeedBusterServiceResponse refreshData() {
        return refreshApplicationContentService.refreshApplicationData();
    }

    @Override
    public FeedBusterServiceResponse getAllEntries(Integer maxResults) {
        return entriesRetrieverService.getStoredFeedEntries(maxResults);
    }

    @Override
    public FeedBusterServiceResponse addNewFeed(Feed feed) {
        return feedInsertService.insert(feed);
    }
}
