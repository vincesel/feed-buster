package com.feed.buster.services;

import com.feed.buster.interfaces.FeedStreamReader;
import com.feed.buster.model.Feed;
import com.feed.buster.model.response.ResponseCode;
import com.feed.buster.model.response.ResponseExitStatus;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import com.feed.buster.persistence.dao.FeedsDAO;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class FeedInsertService {
    
    private static final Logger LOGGER = Logger.getLogger(FeedInsertService.class.getName());
    
    @Autowired
    private FeedsDAO feedsDAO;
    
    private ResponseExitStatus responseStatus;
    private FeedBusterServiceResponse responseWrapper;
    private Feed feed;
    
    public FeedBusterServiceResponse insert(Feed feed) {
        initParams(feed);
        verifyFormatTypeSupport();
        storeFeed();
        buildServiceResponse();
        return responseWrapper;
    }
    
    private void initParams(Feed feed) {
        this.feed = feed;
    }
    
    private void verifyFormatTypeSupport() {
        FeedStreamReader reader = new FeedReaderFactory().getReader(feed.getFormatType());
    }
    
    private void storeFeed() {
        feedsDAO.saveFeed(feed);
        LOGGER.info("Feed successfully saved!");
    }
    
    private void buildServiceResponse() {
        responseStatus = new ResponseExitStatus.ResponseStatusBuilder()
                .withMessages(Arrays.asList("Feed successfully saved."))
                .withResponseCode(ResponseCode.OK)
                .build();
        
        responseWrapper = new FeedBusterServiceResponse.ResponseWrapperBuilder()
                .withStatus(responseStatus)
                .withPayload(Optional.empty())
                .build();
    }
    
}
