package com.feed.buster.services;

import com.feed.buster.exceptions.UnsupportedFeedFormatException;
import com.feed.buster.interfaces.FeedStreamReader;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.services.readers.XmlToSyndFeedReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author vincenzo
 */
@Service
public class FeedReaderFactory {
    
    private static final Logger LOGGER = Logger.getLogger(FeedReaderFactory.class.getName());
    
    public FeedReaderFactory() {};
    
    public FeedStreamReader getReader(FeedFormatType formatType) {
        
        FeedStreamReader reader = null;
        
        switch(formatType) {
            case XML: 
                reader = new XmlToSyndFeedReader();
                break;
            case xml:
                reader = new XmlToSyndFeedReader();
                break;
            default:
                throw new UnsupportedFeedFormatException("Unsupported feed format input.");
        }
        
        LOGGER.log(Level.INFO, "Selected implementation: {0}", reader.getClass().getName());
        return reader;
    }
    
}
