package com.feed.buster.model;

/**
 *
 * @author vincenzo
 */
public enum FeedFormatType {
    
    XML("XML"),xml("xml"),JSON("JSON"),json("json");
    
    private final String id;
    
    public String getId(FeedFormatType formatType) {
       return id;
    }
    
    private FeedFormatType (String id) {
        this.id = id;
    }
    
}
