package com.feed.buster.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;

/**
 *
 * @author vincenzo
 */
public final class FeedEntry {
    
    private final String resourceUri;
    private final String title;
    private final String description;
    private final Date publicationDate;
    private final String imageUrl;
    @JsonIgnore
    private final String sourceId;
    
    private FeedEntry(FeedEntryBuilder builder) {
        this.title = builder._title;
        this.description = builder._description;
        this.publicationDate = builder._publicationDate;
        this.imageUrl = builder._imageUrl;
        this.resourceUri = builder._resourceUri;
        this.sourceId = builder._sourceId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }
    
    public String getResourceUri() {
        return resourceUri;
    }
    
    public String getSourceId() {
        return sourceId;
    }
    
    public static final class FeedEntryBuilder {
        String _title;
        String _description;
        Date _publicationDate;
        String _imageUrl;
        String _resourceUri;
        String _sourceId;
        
        public FeedEntryBuilder withTitle(String title) {
            _title = title;
            return this;
        }
        
        public FeedEntryBuilder withDescription(String description) {
            _description = description;
            return this;
        }
        
        public FeedEntryBuilder withPubblicationDate(Date publicationDate) {
            _publicationDate = publicationDate;
            return this;
        }
        
        public FeedEntryBuilder withImageUrl(String imageUrl) {
            _imageUrl = imageUrl;
            return this;
        }
        
        public FeedEntryBuilder withResourceUri(String resourceUri) {
            _resourceUri = resourceUri;
            return this;
        }
        
        public FeedEntryBuilder withSourceId(String sourceId) {
            _sourceId = sourceId;
            return this;
        }
        
        public FeedEntry build() {
            return new FeedEntry(this);
        }
    }
}
