package com.feed.buster.model;

import com.feed.buster.exceptions.MalformedFeedUrlException;
import java.net.MalformedURLException;
import java.net.URL;
import org.hibernate.annotations.Entity;

/**
 *
 * @author vincenzo
 */
@Entity
public final class Feed {
    
    private final String url;
    private final FeedFormatType formatType;
    
    private Feed (FeedBuilder builder){
        url = builder._url;
        formatType = builder._formatType;
    }
    
    public String getUrl() {
        return url;
    }

    public FeedFormatType getFormatType() {
        return formatType;
    }
    
    public static final class FeedBuilder {
        
        String _url;
        FeedFormatType _formatType;
        
        public FeedBuilder withUrl(String url) {
            _url = url;
            return this;
        }
        
        public FeedBuilder withFormatType(FeedFormatType formatType) {
            _formatType = formatType;
            return this;
        }
        
        public Feed build() {
            validate(this);
            return new Feed(this);
        }
        
        private void validate(FeedBuilder builder) {
            URL feedSource = null;
            try {
                feedSource = new URL(builder._url);
            } 
            catch (MalformedURLException ex) {
                 throw new MalformedFeedUrlException(ex.getMessage());   
            }
        }
       
    }

}
