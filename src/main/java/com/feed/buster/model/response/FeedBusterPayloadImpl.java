package com.feed.buster.model.response;

import com.feed.buster.interfaces.FeedBusterPayload;

/**
 *
 * @author vincenzo
 */
public class FeedBusterPayloadImpl implements FeedBusterPayload<Object>{

    private final Object payload;
    
    private FeedBusterPayloadImpl (PayloadBuilder builder) {
        this.payload = builder._object;
    }
    
    @Override
    public Object getServicePayload() {
        return payload;
    }
    
    public static final class PayloadBuilder {
        Object _object;
        
        public PayloadBuilder withPayload(Object object){
            _object = object;
            return this;
        }
        
        public FeedBusterPayloadImpl build() {
            return new FeedBusterPayloadImpl(this);
        }
    }
    
}
