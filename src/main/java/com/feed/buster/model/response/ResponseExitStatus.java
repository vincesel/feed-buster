package com.feed.buster.model.response;

import java.util.List;

/**
 *
 * @author vincenzo
 */
public class ResponseExitStatus {
    
    private final ResponseCode responseCode;
    private final List<String> messages;
    
    private ResponseExitStatus(ResponseStatusBuilder builder) {
        this.responseCode = builder._responseCode;
        this.messages = builder._messages;
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    public List<String> getMessages() {
        return messages;
    }    
    
    public static final class ResponseStatusBuilder {
        
        List<String> _messages;
        ResponseCode _responseCode;
        
        public ResponseStatusBuilder withMessages(List<String> messages) {
            _messages = messages;
            return this;
        }
        
        public ResponseStatusBuilder withResponseCode(ResponseCode responseCode) {
            _responseCode = responseCode;
            return this;
        }
        
        public ResponseExitStatus build() {
            return new ResponseExitStatus(this);
        }
    }
}
