package com.feed.buster.model.response;

import com.feed.buster.interfaces.FeedBusterPayload;
import java.util.Optional;

/**
 *
 * @author vincenzo
 */
public class FeedBusterServiceResponse {
    
    private final Optional<FeedBusterPayload<? extends Object>> payload; 
    private final ResponseExitStatus responseStatus;
    
    private FeedBusterServiceResponse(ResponseWrapperBuilder builder) {
        this.payload = builder._payload;
        this.responseStatus = builder._responseStatus;
    }

    public Optional<FeedBusterPayload<? extends Object>> getPayload() {
        return payload;
    }

    public ResponseExitStatus getResponseStatus() {
        return responseStatus;
    }
    
    public static final class ResponseWrapperBuilder {
        
        Optional<FeedBusterPayload<? extends Object>> _payload;
        ResponseExitStatus _responseStatus;
        
        public ResponseWrapperBuilder withPayload(Optional<FeedBusterPayload<? extends Object>> payload) {
            _payload = payload;
            return this;
        }
        
        public ResponseWrapperBuilder withStatus(ResponseExitStatus responseStatus) {
            _responseStatus = responseStatus;
            return this;
        }
        
        public FeedBusterServiceResponse build() {
            return new FeedBusterServiceResponse(this);
        }
    }
    
}
