package com.feed.buster.model;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author vincenzo
 */
public class AboutTheProject {
    
    public final List<String> projectTechnologiesStack = Arrays.asList(
            "Java 8","Spring Framework","REST services",
            "Hibernate","JPA","DI","Maven","H2 DB",
            "JSON","XML","JUnit","Mockito","ROME",
            "SWAGGER","Clean code","Design Patterns","GIT"
    );
    
}
