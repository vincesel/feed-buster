package com.feed.buster.model;

/**
 *
 * @author vincenzo
 */
public class MessageConstants {
    
    // Exceptions
    public static final String MISSING_SERVLET_REQUEST_PARAMETER_EXCEPTION_MESSAGE = "Service called with mandatory parameter(s) not setted.";
    public static final String HTTP_REQUEST_METHOD_NOT_SUPPORTED_EXCEPTION_MESSAGE = "Unsupported request method.";
    public static final String MALFORMED_FEED_URL_EXCEPTION_MESSAGE = "The specified URL is malformed. Verify inserted data and try again.";
    public static final String RUNTIME_EXCEPTION_MESSAGE = "Error. Please try again.";
    public static final String UNKNOWN_HOST_EXCEPTION_MESSAGE = "Impossible to reach specified feed source. (You need a working internet connection in order to refresh the feeds).";
    public static final String FEED_READER_CREATION_EXCEPTION_MESSAGE = "Error verified while building feed reader.";
    public static final String UNSUPPORTED_FEED_FORMAT_EXCEPTION_MESSAGE = "Service called with mandatory parameter(s) not setted.";
    
}
