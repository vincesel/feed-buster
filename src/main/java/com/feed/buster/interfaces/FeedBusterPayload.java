package com.feed.buster.interfaces;

/**
 *
 * @author vincenzo
 */

public interface FeedBusterPayload<T> {
    
    T getServicePayload();
    
}
