package com.feed.buster.interfaces;

import com.sun.syndication.feed.synd.SyndFeed;

/**
 *
 * @author vincenzo
 */
public interface FeedStreamReader {
    
    public SyndFeed getFeedFrom(String sourceURL);
    
}
