package com.feed.buster.interfaces;

import com.feed.buster.model.Feed;
import com.feed.buster.model.response.FeedBusterServiceResponse;

/**
 *
 * @author vincenzo
 */
public interface ApplicationManager {
    
    public FeedBusterServiceResponse refreshData();

    public FeedBusterServiceResponse getAllEntries(Integer maxResults);
    
    public FeedBusterServiceResponse addNewFeed(Feed feed);
    
}
