package com.feed.buster.controllers;

import com.feed.buster.exceptions.UnsupportedFeedFormatException;
import com.feed.buster.interfaces.ApplicationManager;
import com.feed.buster.model.AboutTheProject;
import com.feed.buster.model.Feed;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import io.swagger.annotations.ApiOperation;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vincenzo
 */
@RestController
@RequestMapping("/feeds")
public class FeedBusterController {
    
    private static final Logger LOGGER = Logger.getLogger(FeedBusterController.class.getName());
    
    @Autowired
    ApplicationManager applicationManager;
    
    @ApiOperation("Read stored news.")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody FeedBusterServiceResponse getFeedEntries (
        @RequestParam(value = "max_results", required = false) Integer maxResults) 
    {
        LOGGER.log(Level.INFO, "Incoming request: /get-feeds.");
        return applicationManager.getAllEntries(maxResults);
    }
    
    @ApiOperation("Add a new feed to existings sources.")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = {"application/json"})
    public @ResponseBody FeedBusterServiceResponse addNewFeed (
        @RequestParam(value = "format", required=true) String format,
        @RequestParam(value = "feed_url", required=true) String feedUrl)
    {
        LOGGER.log(Level.INFO, "Incoming request: /add-new-feed.");
        
        FeedFormatType formatType;
        try {
            formatType = FeedFormatType.valueOf(format);
        }
        catch (IllegalArgumentException ex) {
            throw new UnsupportedFeedFormatException();
        }
        
        Feed inputFeed = new Feed.FeedBuilder()
                .withUrl(feedUrl)
                .withFormatType(formatType)
                .build();
        
        return applicationManager.addNewFeed(inputFeed);
    }
    
    @ApiOperation("Force application to refresh stored content.")
    @RequestMapping(value = "/refresh", method = RequestMethod.PUT, produces = {"application/json"})
    public @ResponseBody FeedBusterServiceResponse refreshContent () {
        LOGGER.log(Level.INFO, "Incoming request: /refresh-content");
      
        return applicationManager.refreshData();
    }
    
    @ApiOperation("Get info about used technology stack and others author's skills.")
    @RequestMapping(value = "/project", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody AboutTheProject getMoreInfo() {
        LOGGER.log(Level.INFO, "Incoming request: /project");
      
        return new AboutTheProject();
    }
    
}
