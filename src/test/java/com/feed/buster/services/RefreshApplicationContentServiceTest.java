package com.feed.buster.services;

import com.feed.buster.model.Feed;
import com.feed.buster.model.FeedEntry;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.persistence.dao.FeedEntriesDAO;
import com.feed.buster.persistence.dao.FeedsDAO;
import com.feed.buster.services.readers.XmlToSyndFeedReader;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author vincenzo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RefreshApplicationContentServiceTest {
    
    @Mock
    FeedEntriesDAO feedEntryDao;
    @Mock
    FeedsDAO feedDao;
    @Mock
    DefineDefaultFeedSourceService defineDefaultFeedSourceService;
    @Mock
    XmlToSyndFeedReader feedSyndReader;
    @Mock
    FeedReaderFactory feedReaderFactory;
    
    @Autowired
    RefreshApplicationContentService refreshApplicationContentService;
    
    Feed mockedSourceFeed;
    List<FeedEntry> mockedEntries; 
    List<Feed> mockedFeeds;
    SyndFeedImpl feedReader;
    List<SyndEntryImpl> syndEntriesList;
    
    @Before
    public void init() {
        mockedFeeds = new ArrayList<>();
        
        mockedSourceFeed = new Feed.FeedBuilder()
                .withFormatType(FeedFormatType.XML)
                .withUrl("http://testurl.com")
                .build();
        
        mockedFeeds.add(mockedSourceFeed);
        
        mockedEntries = new ArrayList<>();
        
        FeedEntry feedEntry1 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 1")
                .withImageUrl("image1.png")
                .withPubblicationDate(new Date(2019, 05, 15))
                .withResourceUri("uri 1")
                .withTitle("Title 1")
                .build();
        
        FeedEntry feedEntry2 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 2")
                .withImageUrl("image2.png")
                .withPubblicationDate(new Date(2019, 05, 12))
                .withResourceUri("uri 2")
                .withTitle("Title 2")
                .build();
        
        FeedEntry feedEntry3 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 3")
                .withImageUrl("image3.png")
                .withPubblicationDate(new Date(2019, 05, 10))
                .withResourceUri("uri 2")
                .withTitle("Title 2")
                .build();
     
        mockedEntries.add(feedEntry1);
        mockedEntries.add(feedEntry2);
        mockedEntries.add(feedEntry3);
        
        feedReader = new SyndFeedImpl();
        feedReader.setUri(mockedSourceFeed.getUrl());
        
        syndEntriesList = new ArrayList<>();
        mockedEntries.stream()
                .forEach(entry -> syndEntriesList.add(getSyndEntryFromFeedEntry(entry)));
        feedReader.setEntries(syndEntriesList);
        
        when(feedDao.getFeeds()).thenReturn(mockedFeeds);
        when(feedReaderFactory.getReader(FeedFormatType.XML)).thenReturn(feedSyndReader);
        when(feedSyndReader.getFeedFrom(mockedSourceFeed.getUrl())).thenReturn(feedReader);
        when(defineDefaultFeedSourceService.configureApplicationDefaultSourceFeed()).thenReturn(mockedSourceFeed);
    }
    
    private SyndEntryImpl getSyndEntryFromFeedEntry(FeedEntry feedEntry) {
        SyndEntryImpl newSyndEntry = new SyndEntryImpl();
        
        newSyndEntry.setTitle(feedEntry.getTitle());
        
        SyndContentImpl syndContent = new SyndContentImpl();
        syndContent.setValue(feedEntry.getDescription());
        newSyndEntry.setDescription(syndContent);
        
        SyndEnclosureImpl enclosure = new SyndEnclosureImpl();
        enclosure.setUrl(feedEntry.getImageUrl());
        newSyndEntry.setUri(feedEntry.getResourceUri());
        ArrayList<SyndEnclosureImpl> enclosuresList = new ArrayList<>();
        enclosuresList.add(enclosure);
        newSyndEntry.setEnclosures(enclosuresList);
        newSyndEntry.setPublishedDate(feedEntry.getPublicationDate());
        
        return newSyndEntry;
    }
    
    @Test
    public void refreshApplicationContentWithCorrectItems() {
        refreshApplicationContentService.refreshApplicationData();
        
        assertTrue(refreshApplicationContentService.getScheduledWork().isRunning());
        assertTrue(refreshApplicationContentService.getScheduledWork().isRepeats());
    }
    
}
