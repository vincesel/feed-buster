package com.feed.buster.services;

import com.feed.buster.model.FeedEntry;
import com.feed.buster.model.response.ResponseCode;
import com.feed.buster.model.response.FeedBusterServiceResponse;
import com.feed.buster.persistence.dao.FeedEntriesDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author vincenzo
 */
@RunWith(MockitoJUnitRunner.class)
public class FeedEntriesRetrieverServiceTest {
    
    @Mock
    FeedEntriesDAO feedEntriesDAO;
    
    @InjectMocks
    FeedEntriesRetrieverService feedEntriesRetrieverService;
    
    private List<FeedEntry> mockedEntries;
    
    @Before
    public void init(){
        
        mockedEntries = new ArrayList();
        
        FeedEntry feedEntry1 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 1")
                .withImageUrl("image1.png")
                .withPubblicationDate(new Date(2019, 05, 15))
                .withResourceUri("uri 1")
                .withTitle("Title 1")
                .build();
        
        FeedEntry feedEntry2 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 2")
                .withImageUrl("image2.png")
                .withPubblicationDate(new Date(2019, 05, 12))
                .withResourceUri("uri 2")
                .withTitle("Title 2")
                .build();
        
        FeedEntry feedEntry3 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 3")
                .withImageUrl("image3.png")
                .withPubblicationDate(new Date(2019, 05, 10))
                .withResourceUri("uri 2")
                .withTitle("Title 2")
                .build();
      
        mockedEntries.add(feedEntry1);
        mockedEntries.add(feedEntry2);
        mockedEntries.add(feedEntry3);
    }
    
    @Test
    public void retrieveStoredEntries() {
        
        when(feedEntriesDAO.getStoredEntries(Integer.MAX_VALUE)).thenReturn(mockedEntries);
        
        FeedBusterServiceResponse wrapper = feedEntriesRetrieverService.getStoredFeedEntries(Integer.MAX_VALUE);
                
        List<FeedEntry> retrievedEntries = (List<FeedEntry>) wrapper.getPayload().get().getServicePayload();
        assertEquals(3, retrievedEntries.size());
        assertTrue(wrapper.getResponseStatus().getResponseCode().equals(ResponseCode.OK));
    }
}
