package com.feed.buster.services;

import com.feed.buster.exceptions.UnsupportedFeedFormatException;
import com.feed.buster.interfaces.FeedStreamReader;
import com.feed.buster.model.Feed;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.services.readers.XmlToSyndFeedReader;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author vincenzo
 */
@RunWith(MockitoJUnitRunner.class)
public class FeedReaderFactoryTest {
    
    @InjectMocks
    FeedReaderFactory feedReaderFactory;
    
    Feed mockedSourceFeed;
    
    @Before
    public void init() {
        mockedSourceFeed = new Feed.FeedBuilder()
                .withFormatType(FeedFormatType.XML)
                .withUrl("http://testurl.com")
                .build();
    }
    
    @Test
    public void getCoherentImplementationForFeed() {
        FeedStreamReader streamReader = feedReaderFactory.getReader(FeedFormatType.XML);
        
        assertTrue(streamReader.getClass().equals(XmlToSyndFeedReader.class));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void tryToGetReaderWithWrongFormatTypeParameter() {
        feedReaderFactory.getReader(FeedFormatType.valueOf("format"));
    }
    
    @Test(expected = UnsupportedFeedFormatException.class)
    public void tryToInstantiateAJsonFeedReader (){
        feedReaderFactory.getReader(FeedFormatType.JSON);
    }
    
}
