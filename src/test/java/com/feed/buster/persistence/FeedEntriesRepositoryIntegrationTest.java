package com.feed.buster.persistence;

import com.feed.buster.model.FeedEntry;
import com.feed.buster.persistence.dao.FeedEntriesDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author vincenzo
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class FeedEntriesRepositoryIntegrationTest {
    
    @Autowired
    TestEntityManager entityManager;
    
    @Autowired
    FeedEntriesDAO entriesDao;
    
    List<FeedEntry> entriesToStore;
    FeedEntry feedEntry1;
    FeedEntry feedEntry2;
    FeedEntry feedEntry3;
    
    @Before
    public void init(){
        
        feedEntry1 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 1")
                .withImageUrl("uri1.test/image1.png")
                .withPubblicationDate(new Date(2019, 05, 15))
                .withResourceUri("uri1.test")
                .withTitle("Title 1")
                .build();
        
        feedEntry2 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 2")
                .withImageUrl("uri2.test/image2.png")
                .withPubblicationDate(new Date(2019, 05, 12))
                .withResourceUri("uri2.test")
                .withTitle("Title 2")
                .build();
        
        feedEntry3 = new FeedEntry.FeedEntryBuilder()
                .withDescription("Description 3")
                .withImageUrl("uri3.test/image3.png")
                .withPubblicationDate(new Date(2019, 05, 10))
                .withResourceUri("uri3.test")
                .withTitle("Title 3")
                .build();
        
        entriesToStore = new ArrayList<>();
        entriesToStore.addAll( Arrays.asList(feedEntry1, feedEntry2, feedEntry3) );
    }
    
    @Test
    public void whenFindAll_retrieveInsertedEntries() {
        entriesDao.saveFeedEntries(entriesToStore);
        
        List<FeedEntry> readedEntries = entriesDao.getStoredEntries();
        

        assertTrue(readedEntries.stream()
                .map(
                    readedEntry -> readedEntry.getResourceUri())
                .allMatch(
                    entryResourceUri -> matchWithInsertedResourceUris(entryResourceUri))
        );
        
        assertEquals(3, readedEntries.size());
    }
    
    private boolean matchWithInsertedResourceUris(String readedUri) {
        List<String> storedUris = entriesToStore.stream()
                .map(
                    entry -> entry.getResourceUri())
                .collect(Collectors.toList());
        
        return storedUris.contains(readedUri);
    }
    
    @Test
    public void insertingNewEntryWithExistingId_secondInsertShouldBeTranslatedInUpdateOnAlreadyStoredEntriesId() {
        
        entriesDao.saveFeedEntries(entriesToStore);
    
        FeedEntry newFeedEntry = new FeedEntry.FeedEntryBuilder()
                .withDescription("New Description")
                .withImageUrl("uri3.test/newimage.png")
                .withPubblicationDate(new Date(2019, 06, 15))
                .withResourceUri("uri3.test")
                .withTitle("Title 3")
                .build();
        
        entriesToStore = Arrays.asList(newFeedEntry);
        
        entriesDao.saveFeedEntries(entriesToStore);
        
        List<FeedEntry> readedEntries = entriesDao.getStoredEntries();
        
        assertEquals(3, readedEntries.size());
        
        Optional<FeedEntry> updatedEntry = readedEntries.stream()
                .filter( 
                    entity -> entity.getResourceUri().equals("uri3.test"))
                .findAny();
        
        assertTrue(updatedEntry.isPresent());
        assertTrue(updatedEntry.get().getDescription().equals("New Description"));
        assertTrue(updatedEntry.get().getImageUrl().equals("uri3.test/newimage.png"));
    }
}
