package com.feed.buster.persistence;

import com.feed.buster.model.Feed;
import com.feed.buster.model.FeedFormatType;
import com.feed.buster.persistence.dao.FeedsDAO;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author vincenzo
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class FeedRepositoryIntegrationTest {
    
    @Autowired
    TestEntityManager entityManager;
    
    @Autowired
    FeedsDAO entriesDao;
    
    Feed newFeed;
    
    @Before
    public void init() {
        newFeed = new Feed.FeedBuilder()
            .withFormatType(FeedFormatType.XML)
            .withUrl("http://feed1.test")
            .build();
    }
    
    @Test
    public void insertNewFeed_retrieveItFromStoredFeeds(){
        entriesDao.saveFeed(newFeed);
        
        List<Feed> storedFeeds = entriesDao.getFeeds();
        
        assertEquals(1, storedFeeds.size());
        assertTrue(
                storedFeeds.stream()
                .allMatch(
                        feed -> feed.getUrl().equals(newFeed.getUrl()) 
                                && 
                                feed.getFormatType().equals(newFeed.getFormatType())
                )
        );
    }
}
